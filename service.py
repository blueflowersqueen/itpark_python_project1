import db


def check_user(user_id: int):
    user = db.get_user(user_id)
    if user is not None:
        return user
    return None


def add_user(user_id: int, name: str):
    db.add_user(user_id, name)


def add_user_if_not_exist(user_id: int, name: str):
    checked_user = check_user(user_id)
    if checked_user is None:
        add_user(user_id, name)
    else:
        if checked_user.is_deleted:
            checked_user.is_deleted = False
            db.soft_add_user_by_id(checked_user.id)


def soft_delete_user_by_id(user_id: int):
    db.soft_delete_user_by_id(user_id)


def process_button_message(message: str):
    if message == "notes":
        return 'Нотес'

    elif message == "events":
        return 'Это походу мероприятия'

    elif message == "setup":
        return 'Я не знаю что за сетуп'

    elif message == "info":
        return 'Эт вы нажали инфо'

    elif message == "кнопка":
        return 'Ну вы нажали кнопку'

    else:
        return None
