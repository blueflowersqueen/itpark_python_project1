from dotenv import dotenv_values, find_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from user import User

CONFIG = dotenv_values(find_dotenv(".env"))
conn_string = f'postgresql+psycopg2://{CONFIG.get("DB_USER_NAME")}:{CONFIG.get("DB_USER_PASS")}' \
              f'@{CONFIG.get("DB_HOST")}:{CONFIG.get("DB_PORT")}/{CONFIG.get("DB_NAME")}'
ENGINE = create_engine(conn_string, echo=False)
SESSION = sessionmaker(bind=ENGINE)


def add_user(user_id: int, name: str):
    session = SESSION()
    session.add(User(id=user_id, name=name))
    session.commit()


def soft_delete_user_by_id(user_id: int):
    session = SESSION()
    user = get_user(user_id, session=session)
    user.is_deleted = True
    session.add(user)
    session.commit()


def get_user(user_id: int, session=None) -> User:
    if session is None:
        session = SESSION()
    user = session.query(User).filter_by(id=user_id).first()
    return user


def soft_add_user_by_id(user_id: int):
    session = SESSION()
    user = get_user(user_id, session=session)
    user.is_deleted = False
    session.commit()
