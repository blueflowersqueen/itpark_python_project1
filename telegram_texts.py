WELCOME_MESSAGE = "\nЯ бот для заметок, давай познакомимся поближе!"\
                  "\nЧтобы получить список моих возможностей введите команду - " + "/info" + \
                  "\nДля помощи и исправления багов обращайтесь к @kondrartyom"

#language=HTML
BOT_COMMANDS_MESSAGE = '<b>У меня есть следующие команды:</b>' \
                       '\n<b>/start - </b>начать все сначала.\n' \
                       '\n<b>/off - </b>остановить бота.\n' \
                       '\n<b>/lil - </b>сюрприз.\n'

STICKERS = {
    0: 'CAACAgIAAxkBAAMoXpzP3CWCkQxsVDLY5hEUGBGOuc0AAvgAAwkSNAABLUsVuOP-dWQYBA',
    1: 'CAACAgIAAxkBAAMuXpzRGeswmw_N25IHrPx8GGGVddAAAmMDAAJS-REHdXjf1JTAZtYYBA',
    2: 'CAACAgIAAxkBAAM0XpzS1WiBaKvY7EhHRb_1dGt9w6sAAqUFAAJTsfcDr2zCCbbAFqYYBA',
    3: 'CAACAgIAAxkBAAM4XpzTIJ1-QEUEhUEga0IsvJ1uQsQAAhUAA3Xtxh-EZKYCEPTsYxgE',
    4: 'CAACAgIAAxkBAAK_NF69x5obwRkQ6seLq4FOKXnXnCEIAAISAAO0D9kVYGWn6sMwOp4ZBA',
    5: 'CAACAgIAAxkBAAM6XpzTKYWGVJW1SUqFEGj_uCEOCs4AAgEAA3Xtxh-GBXqzY9NeWhgE',
    6: 'CAACAgIAAxkBAAK_LF69x2i7IW-JgfyxAicr2TTMDGH7AAIBAAN17cYfhgV6s2PTXloZBA',
    7: 'CAACAgIAAxkBAAK_Ll69x3NtshYFo3cF0CrEBUDq1Ub1AAIPAAN17cYf_Fe6qaMBp1QZBA',
    8: 'CAACAgIAAxkBAAK_Ql69yFHRjJSk-D4nH_9DdRrRC6FdAAJcAANFqmQMQ3oxkFr5F8AZBA'
}

UNCLEAR_MESSAGE = '<b>Давай еще раз ;)</b>\nЧто я должен сделать? '

COMMAND_PHRASE = "\nЧтобы начать работу - выберете что сделать"
BOT_LIL_MESSAGE = '\nЭто вы нажали Лил, а это я - @lil_red_witch'
