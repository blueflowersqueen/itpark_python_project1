from telegram_texts import WELCOME_MESSAGE


def hi_mess(name: str):
    """Вывод приветственного уведомления"""
    return "Привет, " + name + " !" + WELCOME_MESSAGE


def get_user_name(message):
    if (message.from_user.first_name is not None) & (message.from_user.last_name is not None):
        name = message.from_user.first_name

    elif (message.from_user.first_name is not None) & (message.from_user.last_name is None):
        name = message.from_user.first_name

    elif message.from_user.username is not None:
        name = message.from_user.username

    else:
        name = 'Пользователь'

    return name
