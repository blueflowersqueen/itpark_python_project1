from dotenv import dotenv_values, find_dotenv
import random

from dotenv import dotenv_values, find_dotenv
from telebot import TeleBot, types

from service import add_user_if_not_exist, soft_delete_user_by_id, process_button_message
from telegram_helpers import get_user_name, hi_mess
from telegram_texts import BOT_COMMANDS_MESSAGE, STICKERS, UNCLEAR_MESSAGE, \
    COMMAND_PHRASE

CONFIG = dotenv_values(find_dotenv(".env"))
TOKEN = CONFIG.get('TOKEN')
bot = TeleBot(TOKEN)


def random_sticker(message):
    """Вывод случайного"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    bot.send_sticker(message.chat.id, STICKERS[random.randrange(0, 8)], reply_markup=markup)


def button_markup():
    """Создания кнопок на клавиатуре"""
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    btn1 = types.KeyboardButton('Notes')
    btn2 = types.KeyboardButton('Events')
    btn3 = types.KeyboardButton('Setup')
    btn4 = types.KeyboardButton('Info')
    btn5 = types.KeyboardButton('Кнопка')
    markup.add(btn1, btn2, btn3, btn4, btn5)
    return markup


@bot.message_handler(commands=['start'])
def start(message):
    """Запуск бота"""
    name = get_user_name(message)
    add_user_if_not_exist(message.from_user.id, name)
    bot.send_message(message.chat.id, hi_mess(name), parse_mode='html')
    bot.send_message(message.chat.id, COMMAND_PHRASE, parse_mode='html', reply_markup=button_markup())


@bot.message_handler(commands=['off'])
def off(message):
    """Остановка бота"""
    soft_delete_user_by_id(message.from_user.id)


@bot.message_handler(commands=['info'])
def info(message):
    """Вывод списка имеющихся команд"""
    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    bot.send_message(message.chat.id, BOT_COMMANDS_MESSAGE, parse_mode='html', reply_markup=button_markup())


@bot.message_handler(content_types=['text'])
def write_all(message):
    """Обработка клавиатурных кнопок"""

    add_user_if_not_exist(message.from_user.id, get_user_name(message))
    get_message_text = message.text.strip().lower()
    callback_message = process_button_message(get_message_text)
    if callback_message is None:
        random_sticker(message)
        bot.send_message(message.chat.id, UNCLEAR_MESSAGE, parse_mode='html', reply_markup=button_markup())
    else:
        bot.send_message(message.chat.id, callback_message, parse_mode='html', reply_markup=button_markup())


@bot.message_handler(content_types='sticker')
def sticker(message):
    bot.send_sticker(message.chat.id, message.sticker.file_id, reply_markup=button_markup())
    bot.send_message(message.chat.id, "Я тоже так могу!!!", parse_mode='html', reply_markup=button_markup())

bot.polling(none_stop=True)